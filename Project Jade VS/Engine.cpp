#include "Engine.h"
#include "Adafruit_MPU6050.h"
#include <Wire.h>

// setting PWM properties
Adafruit_MPU6050 mpu;
//Adafruit_Sensor* mpu_temp, * mpu_accel, * mpu_gyro;
float xOld, yOld, zOld;
//sensors_event_t a, aOld, g, temp;

Mountain m;
ButtonTester bt;

namespace Engine {
	Display Lcd = Display();
	

	bool IsPaused = false;
	uint fps = 30; //minimum is 24
	double TargetDelta = 1000 / fps;
	double lastUpdate = 0;


	Scene* activeScene;
	int activeSceneIndex = 0;
}

void Engine::Init() {
	Serial.begin(115200);
	Serial.println((String)Engine::Lcd.tft.width());
	Serial.println((String)Engine::Lcd.tft.height());
	Lcd.Init();
	Lcd.tft.setRotation(3);

	m.Init();

	//Wire.begin();
	//mpu.begin();
	//mpu.setRange(MPU6050_RANGE_16G);
}

//int buttonPins[7] = { GPIO_26,GPIO_27,GPIO_25,GPIO_33,GPIO_12,GPIO_14, GPIO_32 };
void PrintButtonState() {
	for (size_t i = 0; i < 6; i++) {

	Serial.println("Button: " + (String)i + " is " + (String)digitalRead(Input::buttonPins[i]));
	}
}

double elapsed;
void Engine::GameLoop() {
	if (IsPaused) {
		return;
	}

	double current = millis();
	elapsed += current - lastUpdate;

	//Serial.println("update fps " + (String)(1000/elapsed));

	//Serial.println("curr: " + (String)(current) +" last: " + (String)(lastUpdate));
	//Serial.println(" elapsed: " + (String)(elapsed)+" target: " + (String)(TargetDelta));

	//game updates as much as possible
	EarlyUpdate();
	Update();
	LateUpdate();

	//only updates screen to match fps
	if (elapsed >= TargetDelta) {
		//Serial.println("target delta met");

		Prerender();
		Render();
		Lcd.renderScreenBrightness();
		//Serial.println("render fps " + (String)(1000 / elapsed) + "target"+ (String)(TargetDelta));
		elapsed = 0;

	}

	lastUpdate = current;
}

#pragma region Pedometer code
uint shakeThreshold = 25;
uint maxShakesPerSec = 8;
float timeBetweenShakes = 1000 / maxShakesPerSec;
uint shakes = 0;
bool resetOnChange = true;

float lastStep = 0;
float maxes[4];

void Pedometer() {

	float xAccel = abs(mpu.readScaledAccel().XAxis) - xOld;
	float yAccel = abs(mpu.readScaledAccel().YAxis) - yOld;
	float zAccel = abs(mpu.readScaledAccel().ZAxis) - zOld;

	float mag = sqrt((xAccel * xAccel) + (yAccel * yAccel) + (zAccel * zAccel));


	if (millis() - lastStep >= timeBetweenShakes) {
		if (mag >= shakeThreshold) {
			shakes++;
			lastStep = millis();
		}
	}

	Engine::Lcd.PrintText("Threshold: " + (String)shakeThreshold, 0, 10, AlignCentered);
	Engine::Lcd.PrintText("Max Shakes: " + (String)maxShakesPerSec, 0, 20, AlignCentered);

	Engine::Lcd.PrintText("X: " + (String)xAccel, 0, 40, AlignLeft);
	Engine::Lcd.PrintText("Y: " + (String)yAccel, 0, 50, AlignLeft);
	Engine::Lcd.PrintText("Z: " + (String)zAccel, 0, 60, AlignLeft);
	Engine::Lcd.PrintText("Mag: " + (String)mag, 0, 70, AlignLeft);

	Engine::Lcd.PrintText("X max: " + (String)maxes[0], 0, 40, AlignRight);
	Engine::Lcd.PrintText("Y max: " + (String)maxes[1], 0, 50, AlignRight);
	Engine::Lcd.PrintText("Z max: " + (String)maxes[2], 0, 60, AlignRight);
	Engine::Lcd.PrintText("Mag max: " + (String)maxes[3], 0, 70, AlignRight);

	if (resetOnChange) {
		Engine::Lcd.PrintText("Reset on change", 0, 90, AlignCentered);
	}

	Engine::Lcd.PrintText("Shakes: " + (String)shakes, 0, 100, AlignCentered);


	if (xAccel > maxes[0]) { maxes[0] = xAccel; }
	if (yAccel > maxes[1]) { maxes[1] = yAccel; }
	if (zAccel > maxes[2]) { maxes[2] = zAccel; }
	if (mag > maxes[3]) { maxes[3] = mag; }

	xOld = mpu.readScaledAccel().XAxis;
	yOld = mpu.readScaledAccel().YAxis;
	zOld = mpu.readScaledAccel().ZAxis;
}

void ResetShakes() {
	shakes = 0;
	for (int i = 0; i < 4; i++) {
		maxes[i] = 0;
	}
}

/// <summary>
/// adjust pedometer threshold and max shakes per second.
/// </summary>
/// <param name="threshold">if true changes reflect on threshold value, false reflects on shakes per second</param>
/// <param name="increase">true increases value by 1, false decreases by 1</param>
/// <param name="resetShakes">if true resets shake counter and max recorded values</param>
void UpdatePedometerValues(bool threshold, bool increase, bool resetShakes) {
	if (threshold) {
		if (increase) {
			shakeThreshold++;
		} else {
			if (shakeThreshold > 1) {
				shakeThreshold--;
			}
		}
	} else {
		if (increase) {
			maxShakesPerSec++;
		} else {
			if (maxShakesPerSec > 1) {
				maxShakesPerSec--;
			}
		}

		timeBetweenShakes = 1000 / maxShakesPerSec;
	}

	if (resetShakes) {
		ResetShakes();
	}
}

void PedometerParameterInput() {
	if (Input::ButtonPressed(Up)) {
		UpdatePedometerValues(true, true, resetOnChange);
	}
	if (Input::ButtonPressed(Left)) {
		ResetShakes();
	}
	if (Input::ButtonPressed(Down)) {
		UpdatePedometerValues(true, false, resetOnChange);
	}

	if (Input::ButtonPressed(A)) {
		UpdatePedometerValues(false, true, resetOnChange);
	}
	if (Input::ButtonPressed(C)) {
		UpdatePedometerValues(false, false, resetOnChange);
	}

	if (Input::ButtonPressed(B)) {
		resetOnChange = !resetOnChange;
	}
}

uint16_t colors[4] = { ST7735_WHITE, ST7735_BLUE, ST7735_GREEN, ST7735_RED };
int offset = 95;
void InputTest() {
	Engine::Lcd.tft.fillScreen(ST7735_BLACK);
	Engine::Lcd.tft.DrawEllipse(12, 12, 32, 32, colors[Input::GetButtonState(Up)]);
	Engine::Lcd.tft.DrawEllipse(12, 12, 16, 66, colors[Input::GetButtonState(Left)]);
	Engine::Lcd.tft.DrawEllipse(12, 12, 32, 100, colors[Input::GetButtonState(Down)]);
	Engine::Lcd.tft.DrawEllipse(12, 12, offset + 32, 32, colors[Input::GetButtonState(A)]);
	Engine::Lcd.tft.DrawEllipse(12, 12, offset + 16 + 32, 66, colors[Input::GetButtonState(B)]);
	Engine::Lcd.tft.DrawEllipse(12, 12, offset + 32, 100, colors[Input::GetButtonState(C)]);
}
#pragma endregion

#pragma region Engine loop methods

void Engine::EarlyUpdate() {
	Input::Update();
}

void Engine::Update() {
	//Serial.println("Update called");
	PrintButtonState();
	//Pedometer();
	//PedometerParameterInput();

	bt.DrawScene(&Engine::Lcd);

	//m.UpdateParallax(0);
	//m.DrawScene(&Engine::Lcd);

	//float raw = analogRead(35);

	//Engine::Lcd.PrintText("Power read: " + (String)raw, 0, 10, AlignCentered);
	//Engine::Lcd.PrintText("in volts: " + (String)((raw * 3.3)/4095), 0, 20, AlignCentered);
}

void Engine::LateUpdate() {}

void Engine::Prerender() {}

/// <summary>
/// displays the buffer then clears it to black
/// </summary>
void Engine::Render() {
	Lcd.tft.displayBuffer();
	Engine::Lcd.tft.fillScreen(ST7735_BLACK);
}

#pragma endregion
