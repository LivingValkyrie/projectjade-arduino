#include "Display.h"
#include "GameLogic.h"

void Display::Init() {
	tft.init();
	//tft.setRotation(0);
	//tft.setResolution(50, 50);
	tft.fillScreen(ST7735_BLACK);
	//SetupMenuText();
}

uint16_t color = 0x0000;
void Display::ColorTest() {
	tft.fillScreen(color);
#ifdef SCREEN_BUFFER

	tft.displayBuffer();
#endif
	color += 64;
	Serial.println(color);

	if (color > 0xffff) {
		color = 0;
	}
}

int skybandOffset = -96;
int paralaxOffset = -96;

void Display::DrawScene() {
	//tft.fillRect(0, 0, tft.width(), skyBand, ST7735_CYAN); //skyband
	//tft.fillRect(0, skyBand + 1, tft.width(), background, ST7735_WHITE);
	//tft.fillRect(0, skyBand + background + 1, tft.width(), groundPlane, ST7735_GREEN);
	//tft.fillRect(0, 0, tft.width(), tft.height() - menuBarStart, ST7735_CYAN);

	tft.drawRGBBitmap(paralaxOffset/3, 0, Background, 320, 96); //can paralax, at slow rate
	tft.drawRGBBitmap(paralaxOffset, 112, Ground, 320, 16); //can paralax along with close background
	tft.drawRGBBitmap(paralaxOffset, 52, Trees, 320, 60); //paralax, is close background
	tft.drawRGBBitmap(skybandOffset, 0, Clouds, 320, 36); //can paralax faster than close background

	///old working paralax
	//tft.drawRGBBitmap(skybandOffset, 0, Clouds, 128, skyBandHeight);
	//tft.drawRGBBitmap(skybandOffset - 128, 0, Clouds, 128, skyBandHeight);
	//tft.drawRGBBitmap(0, skyBandHeight, Background, 128, background);
	//tft.drawRGBBitmap(paralaxOffset, skyBandHeight + 40, Trees, 256, 48);
	//tft.drawRGBBitmap(0, skyBandHeight + background + 1, Ground, 128, groundPlane);

	///failed anim code for ground. needs full paralax size
	//tft.drawRGBBitmap(paralaxOffset, skyBand + background + 1, Ground, 128, groundPlane);
	//tft.drawRGBBitmap(-256+ paralaxOffset, skyBand + background + 1, Ground, 128, groundPlane); 
	//tft.drawRGBBitmap(paralaxOffset + 128, skyBand + background + 1, Ground, 128, groundPlane);
	//tft.drawRGBBitmap(-256+paralaxOffset + 128, skyBand + background + 1, Ground, 128, groundPlane);

	paralaxOffset -= xSpeed;
	/*if (paralaxOffset <= -192) {
		paralaxOffset = 0;
	}*/

	skybandOffset--;
	/*if (skybandOffset <= 0) {
		skybandOffset = 128;
	}*/
}

void Display::DrawMenuBar() {
	tft.fillRect(0, menuBarStart, tft.width(), tft.height() - menuBarStart, ST7735_BLACK);
	//tft.FillRoundRect(0, menuBarStart, tft.width(), tft.height() - menuBarStart, 5, ST7735_BLUE);
	//tft.drawChar(0, menuBarStart, 'H', ST7735_BLACK, ST7735_BLUE, 2);
}

MenuState curr;
String menuHeader = "";

void Display::DrawMenuText() {
	//calculate starting point
	int c = (tft.width() / 2) - ((12 * menuHeader.length() / 2)); //start with center, then offset by half of the total word length
	SetupMenuText(c);
	tft.print(menuHeader);

	/*for (int i = 0; i < menuHeader.length(); i++) {
		tft.drawChar(c + i * 12, menuBarStart + 10, menuHeader[i], ST7735_WHITE, ST7735_BLUE, 2);
	}*/
}

void Display::PrintText(String s, int x, int y, Alignment a) {
	int c;
	switch (a) {
	case AlignLeft:
		c = x;
		break;
	case AlignRight:
		c = tft.width() - 6 * s.length();
		break;
	case AlignCentered:
		c = (tft.width() / 2) - ((6 * s.length() / 2)); //start with center, then offset by half of the total word length
		break;
	default:
		c = x;
		break;
	}

	tft.setCursor(c + x, y);
	tft.setTextColor(ST7735_WHITE, ST7735_BLUE);
	tft.setTextSize(1);
	tft.setTextWrap(false);
	tft.print(s);
}

void Display::SetupMenuText(int cursor) {
	tft.setCursor(cursor, menuBarStart + 8);
	tft.setTextColor(ST7735_WHITE, ST7735_BLACK);
	tft.setTextSize(2);
	tft.setTextWrap(false);
}

void Display::UpdateMenuText(char newText) {
	menuText = newText;
	//Serial.print(newText);
	switch (menuText) {
		case 'a':
			menuHeader = "";
			break;
		case 'b':
			menuHeader = "Pet";
			break;
		case 'c':
			menuHeader = "Train";
			break;
		case 'd':
			menuHeader = "Map";
			break;
		case 'e':
			menuHeader = "Items";
			break;
		case 'f':
			menuHeader = "Camp";
			break;
		case 'g':
			menuHeader = "Connect";
			break;
		default:
			break;
	}
}

void Display::Animate() {
	//tft.fillScreen(ST7735_BLACK);
	DrawScene();

	if (flip) {
		if (big) {
			//tft.drawRGBBitmap(X, Y, gloop, FrogWidth, FrogHeight);
			tft.drawRGBBitmap(X, skyBandHeight + background - FrogHeight + 8, gloop, FrogWidth, FrogHeight);

		} else {
			//tft.drawRGBBitmap(X, Y, gloop2, FrogWidth, FrogHeight);
			tft.drawRGBBitmap(X, skyBandHeight + background - FrogHeight + 8, gloop2, FrogWidth, FrogHeight);

		}
	} else {
		if (big) {
			//tft.drawRGBBitmap(X, Y, gloop, FrogWidth, FrogHeight);
			tft.DrawRGBBitmapFlipped(X, skyBandHeight + background - FrogHeight + 8, gloop, FrogWidth, FrogHeight);

		} else {
			//tft.drawRGBBitmap(X, Y, gloop2, FrogWidth, FrogHeight);
			tft.DrawRGBBitmapFlipped(X, skyBandHeight + background - FrogHeight + 8, gloop2, FrogWidth, FrogHeight);

		}
	}

	DrawMenuBar();
	DrawMenuText();

	//tft.fillScreen(0x001f);
#ifdef SCREEN_BUFFER

	tft.displayBuffer();
#endif
	big = !big;

	//X += xSpeed;
	Y += ySpeed;

	if (X >= tft.width() - FrogWidth || X <= 0) {
		xSpeed *= -1;
	}

	if (Y >= menuBarStart - FrogHeight || Y <= 0) {
		ySpeed *= -1;
	}

	delay(1000 / frameRate);
}

Display::Display() {}
