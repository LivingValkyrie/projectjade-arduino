#pragma once

#pragma region GPIO Declarations
//esp32 pinout
#define GPIO_0	0	// pulled up, outputs pwm on boot, Touch1
#define GPIO_1	1	// tx pin, dont use - can be used if needed, but removes serial monitoring
#define GPIO_2	2	// onboard led, Touch2
#define GPIO_3	3	// rx pin, high on boot, dont use - can be used if needed, but removes serial monitoring
#define GPIO_4	4	// OPEN, Touch0
#define GPIO_5	5	// outputs pwm on boot

//do not use
#define GPIO_6	6	// integrated spi flash, eeprom? -- no input
#define GPIO_7	7	// integrated spi flash, eeprom? -- no input, may have been working...
#define GPIO_8	8	// integrated spi flash, eeprom? -- no input
#define GPIO_9	9	// integrated spi flash, eeprom? -- no input
#define GPIO_10	10	// integrated spi flash, eeprom? -- no input
#define GPIO_11	11	// integrated spi flash, eeprom? -- no input

#define GPIO_12	12	// boot fails is pulled high, Touch5

#define GPIO_13 13	// some reason not listed, Touch4 -- no input

#define GPIO_14	14	// pwm at boot, Touch6
#define GPIO_15	15	// pwm at boot, Touch3
#define GPIO_16	16	// OPEN
#define GPIO_17	17	// OPEN
#define GPIO_18	18	// OPEN SCK CLOCK
#define GPIO_19	19	// OPEN

#define GPIO_21	21	// i2c data
#define GPIO_22	22	// i2c clk
#define GPIO_23	23	// OPEN  taken by LCD SDA MOSI

#define GPIO_25	25	// OPEN
#define GPIO_26	26	// OPEN
#define GPIO_27	27	// OPEN, Touch7

#define GPIO_32	32	// OPEN, Touch9
#define GPIO_33	33	// OPEN, Touch8

//no internal resistors
#define GPIO_34	34	// INPUT ONLY
#define GPIO_35	35	// INPUT ONLY
#define GPIO_36	36	// INPUT ONLY
#define GPIO_39	39	// INPUT ONLY
#pragma endregion

//SPI pins
#define TFT_DC		GPIO_5 //2   //device control/register select  
#define TFT_RST		GPIO_17 //4   //set to -1 to use boards reset
#define TFT_CS		37 //was 17	 //chip select - each SPI needs one of these. the rest are shared.
//can be any pin, this is not being used in jade, 
//the connection on the lcd is always grounded. this only wokrs if there is only 1 spi device
//37 is a pin not broken out on the wroom32. if i start to see issues with the lcd then i can switch this to a broken out chip, but things are stable currently

#define SPI_CLK		GPIO_18
#define SPI_MOSI	GPIO_23

//LCD Pins
#define LCD_BRIGHTNESS	GPIO_16 
//must return to these values for pcb to work
//                    gnd vcc sck sda res rs cs led
//pins in order, 1-8: gnd vcc 18  23  17  5  22 16

//Input Pins
#define INPUT_UP	GPIO_4
#define INPUT_DOWN  GPIO_15
#define INPUT_LEFT	GPIO_2

#define INPUT_A		GPIO_14 //i2c buss, needs changed
#define INPUT_B		GPIO_12 //i2c buss, needs changed
#define INPUT_C		GPIO_13 //needs pullup resistor
#define INPUT_RESET //"reset" button - same purpose as digivices, not a reset of the device itself, but the software RESET function

// i2c pins - accelerometer
#define I2C_SDA GPIO_21 //data
#define I2C_SCL GPIO_22 //clock

/*
ADC1_CH0 (GPIO 36)
ADC1_CH1 (GPIO 37)
ADC1_CH2 (GPIO 38)
ADC1_CH3 (GPIO 39)
ADC1_CH4 (GPIO 32)
ADC1_CH5 (GPIO 33)
ADC1_CH6 (GPIO 34)
ADC1_CH7 (GPIO 35)
ADC2_CH0 (GPIO 4)
ADC2_CH1 (GPIO 0)
ADC2_CH2 (GPIO 2)
ADC2_CH3 (GPIO 15)
ADC2_CH4 (GPIO 13)
ADC2_CH5 (GPIO 12)
ADC2_CH6 (GPIO 14)
ADC2_CH7 (GPIO 27)
ADC2_CH8 (GPIO 25)
ADC2_CH9 (GPIO 26)

DAC1 (GPIO25)
DAC2 (GPIO26)

RTC_GPIO0 (GPIO36)
RTC_GPIO3 (GPIO39)
RTC_GPIO4 (GPIO34)
RTC_GPIO5 (GPIO35)
RTC_GPIO6 (GPIO25)
RTC_GPIO7 (GPIO26)
RTC_GPIO8 (GPIO33)
RTC_GPIO9 (GPIO32)
RTC_GPIO10 (GPIO4)
RTC_GPIO11 (GPIO0)
RTC_GPIO12 (GPIO2)
RTC_GPIO13 (GPIO15)
RTC_GPIO14 (GPIO13)
RTC_GPIO15 (GPIO12)
RTC_GPIO16 (GPIO14)
RTC_GPIO17 (GPIO27)

pwm pins, all output pins - 34 and 39 cant be pwn
all gpios can be used as interupts

i2c
GPIO 21 (SDA)
GPIO 22 (SCL)

SPI		MOSI		MISO		CLK			CS
VSPI	GPIO 23		GPIO 19		GPIO 18		GPIO 5
HSPI	GPIO 13		GPIO 12		GPIO 14		GPIO 15

strapping pins, used for bootloading
GPIO 0
GPIO 2
GPIO 4
GPIO 5
GPIO 12
GPIO 15
*/