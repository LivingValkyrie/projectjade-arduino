// 
// 
// 

#include "Sprite.h"

void Sprite::DrawAtPosition(const int x, const int y, Display *lcd) {
	//need to build array, using pointers
	int posX = x, posY = y;
	for (int16_t j = 0; j < height; j++, posY++) {
		for (int16_t i = 0; i < width; i++) {
			//writePixel(posX + i, posY, pgm_read_word(&bitmap[j * w + i]));
			lcd->tft.drawPixel(posX + i, posY, *(gfxPointer + (j * width + i)));
		}
	}
}

Sprite::Sprite(int width, int height, DrawLayer _layer, const unsigned short* _gfxPointer, int originX, int originY):layer(_layer),gfxPointer(_gfxPointer) {
	this->width = width;
	this->height = height;
	this->originX = originX;
	this->originY = originY;
}

void Sprite::MoveTo(int x, int y) {
	posX = x;
	posY = y;
}

//Sprite::Sprite(int width, int height,DrawLayer _layer, int originX, int originY){
//}
