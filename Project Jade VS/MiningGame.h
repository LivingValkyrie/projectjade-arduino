// MiningGame.h
#include "Display.h"

#ifndef _MININGGAME_h
#define _MININGGAME_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class MiningGame {

public:
	static void draw(Display Lcd);

	const static int tileSize = 16;
};

#endif

