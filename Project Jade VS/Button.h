//#ifndef _BUTTON_h
//#define _BUTTON_h

#pragma once

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

class Button {
public:
	Button(int pinNumber);

	bool
		Pressed(),
		Released(),
		Held();

	String State();

	int pinNumber;

	int StateAsInt() {
		int toReturn = 0;

		if (Pressed()) {
			toReturn = 1;
		} else if (Held()) {
			toReturn = 2;
		} else {
			toReturn = 3;
		}


		return toReturn;
	}

	uint16_t StateAsColor();

private:
	bool pressed = false;
};
//#endif

