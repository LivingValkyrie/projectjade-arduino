#pragma once

namespace Jade {
	namespace Engine {
		class List {

			int size, maxSize;
			int* array;
			void alloc_new();

		public:
			List(); //base constructor
			List(int); //base constructor
			List(const List&); //copy constructor

			~List(); //deconstructor

			void Add(int); //add element
			int Length(); //get length of list
			int at(int i); //get entry at index


			//operators
			int operator[](int); //index operator
			List& operator+=(int); //+=
			List& operator-=(int); //-=

			List& operator=(const List&);
		};
	}

}

/*

THESE ARE NOT POINTERS

all of the values stored in this list are actually stored together.
might not mean much when i convert it to a template and am storing function pointers, cus those will be pointers
but i may need to make a new version where they are always pointers, regardless of value type.





*/