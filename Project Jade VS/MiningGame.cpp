// 
// 
// 

#include "MiningGame.h"
#include "Display.h"

void MiningGame::draw(Display Lcd) {
	Lcd.tft.fillScreen(ST7735_BLACK);
	for (int i = 0; i < Lcd.tft.width() / tileSize; i++) {
		for (int j = 0; j < Lcd.tft.height() / tileSize; j++) {
			Lcd.tft.drawRect(i, j, tileSize, tileSize, ST7735_BLUE);
		}
	}

#ifdef SCREEN_BUFFER

	Lcd.tft.displayBuffer();
#endif

}

