#include <TFT.h>
#include "Engine.h"

void setup() {
	Engine::Init();

	Engine::Lcd.setupPWM();
	Serial.begin(115200);

	Input::Init();
}

void loop() {
	Engine::GameLoop();
	//delay(500);
	//RotateDisplay();
}

int rot = 0;
void RotateDisplay() {
	Serial.println("rotating display");

	rot++;
	Engine::Lcd.tft.setRotation(rot);

	Engine::Lcd.tft.displayBuffer();

	delay(500);

}

void DrawSprite(const int x, const int y, Sprite s) {

	int posX = x - s.originX, posY = y - s.originY;
	for (int16_t j = 0; j < s.height; j++, posY++) {
		for (int16_t i = 0; i < s.width; i++) {
			//writePixel(posX + i, posY, pgm_read_word(&bitmap[j * w + i]));
			Engine::Lcd.tft.drawPixel(posX + i, posY, *(s.gfxPointer + (j * s.width + i)));
		}
	}

}
