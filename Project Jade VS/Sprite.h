// Sprite.h

#ifndef _SPRITE_h
#define _SPRITE_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "Display.h"

enum DrawLayer {
	GUI = -1,
	Foreground = 0,
	Pet = 1,
	ParallaxClose = 2,
	ParallaxFar = 3
};

class Sprite {
public:
	int width, height, originX, originY, posX,posY;
	const DrawLayer layer;
	void DrawAtPosition(const int x, const int y, Display *lcd);
	Sprite(int width, int height, DrawLayer _layer, const unsigned short* _gfxPointer, int originX = 0, int originY = 0);
	const unsigned short* gfxPointer;
	void MoveTo(int x, int y);

private:


};


#endif

