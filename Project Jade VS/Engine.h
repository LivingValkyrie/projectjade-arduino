#pragma once
#include "Imports.h"

namespace Engine {
#pragma region Functions
	void Init();
	void EarlyUpdate();
	void Update();
	void LateUpdate();
	void Prerender();
	void Render();
	void GameLoop();
#pragma endregion

	extern Display Lcd;
	extern bool IsPaused;
	extern uint fps;
	extern double TargetDelta;
	extern double lastUpdate;
}