#include "List.h"


namespace Jade {
	namespace Engine {
		List::List() {
			maxSize = 20;
			array = new int[maxSize];
			size = 0;
		}

		List::List(int i) {
			maxSize = i;
			array = new int[maxSize];
			size = 0;
		}

		List::List(const List& list) {
			maxSize = list.maxSize;
			size = list.size;
			array = new int[maxSize];
			for (int i = 0; i < maxSize; i++) {
				array[i] = list.array[i];
			}
		}

		List::~List() {
			delete[] array;
		}

		int List::Length() {
			return size;
		}

		void List::Add(int i) {
			if (size + 1 > maxSize) {
				alloc_new();
			}
			array[size] = i;
			size++;
		}

		int List::operator[](int i) {
			return array[i];
		}

		List& List::operator+=(int i) {
			this->Add(i);
		}

		List& List::operator-=(int) {
			// TODO: insert return statement here
			//still gotta solve this one. prolly best would be to go thru it til find a match,
			//remove that, then copy i+1 into i until i==maxSize - 1, in which case i+1 ==maxSize which doesnt exist
			//so i needs to equal nothing
		}

		List& List::operator=(const List& list) {
			if (this != &list) {
				maxSize = list.maxSize;
				size = list.size;

				delete[] array;
				array = new int[maxSize];

				for (int i = 0; i < maxSize; i++) {
					array[i] = list.array[i];
				}
			}
			return *this;
		}

		int List::at(int i) {
			if (i < size) {
				return array[i];
			} else {
				throw 10;
			}
		}

		void List::alloc_new() {
			maxSize *= 2;
			int* tmp = new int[maxSize]; //makes new array
			for (int i = 0; i < maxSize; i++) { //copy old array
				tmp[i] = array[i];
			}
			delete[] array;
			array = tmp;
		}
	}
}
