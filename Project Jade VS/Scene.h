// Scene.h

#ifndef _SCENE_h
#define _SCENE_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "Sprite.h"
#include "Display.h"
#include "Input.h"

class Scene {
public:
	int entityMax;
	Sprite* entities;

	//entities pointer will be assigned to the first in the child classes sprites array,
	//allowing for access thru an iterator.

	virtual void DrawScene(Display* lcd);
};

class MiniGame : public Scene {
public:

	Sprite allEntities[5] = {
		Sprite(10,10,ParallaxFar,Background),
		Sprite(10,10,ParallaxFar,Background),
		Sprite(10,10,ParallaxFar,Background),
		Sprite(10,10,ParallaxFar,Background),
		Sprite(10,10,ParallaxFar,Background)
	};

	void DrawScene(Display* lcd) override {
		entityMax = 5;
		entities = &allEntities[0];
		Serial.print("draw scene called in mg");
		Scene::DrawScene(lcd);
		/*for (size_t i = 0; i < entityMax; i++) {
			Serial.print(i + " is the index of the current sprite im drawing");

			(entities+1 )->DrawAtPosition(i * 10, i * 10, lcd);
		}*/
	};

};

class ButtonTester : public Scene {

public:

	int offset = 95;
	void DrawScene(Display* Lcd) override {
		
		Lcd->tft.drawCircle(32, 32, 12, Input::ButtonPressedOrHeld(ButtonID::A) ? ST7735_GREEN : ST7735_RED);
		Lcd->tft.drawCircle(16, 66, 12, Input::ButtonPressedOrHeld(ButtonID::B) ? ST7735_GREEN : ST7735_RED);
		Lcd->tft.drawCircle(32, 100, 12,Input::ButtonPressedOrHeld(ButtonID::C) ? ST7735_GREEN : ST7735_RED);		   
		   
		Lcd->tft.drawCircle(offset + 32, 32, 12,		Input::ButtonPressedOrHeld(ButtonID::Down) ? ST7735_GREEN : ST7735_RED);
		Lcd->tft.drawCircle(offset + 16 + 32, 66, 12,	Input::ButtonPressedOrHeld(ButtonID::Left) ? ST7735_GREEN : ST7735_RED);
		Lcd->tft.drawCircle(offset + 32, 100, 12,		Input::ButtonPressedOrHeld(ButtonID::Up) ? ST7735_GREEN : ST7735_RED);

		Scene::DrawScene(Lcd);
	}
};

class Mountain : public Scene {
public:
	Sprite layers[5] = {
		Sprite(320,96,ParallaxFar,Background, -160),
		Sprite(320,16,ParallaxClose,Ground, -160),
		Sprite(320,60,ParallaxClose,Trees, -160),
		Sprite(320,36,Foreground,Clouds, -160),
		Sprite(53,56,Pet,gloop)
	};

	void Init() {
		layers[0].MoveTo(-96, 0);
		layers[1].MoveTo(-96, 112);
		layers[2].MoveTo(-96, 52);
		layers[3].MoveTo(-96, 0);
		layers[4].MoveTo(40, 90);
	}

	void DrawScene(Display* lcd) override {
		entityMax = 5;
		entities = &layers[0];
		//Serial.print("draw scene called in mg");
		Scene::DrawScene(lcd);
		//for (size_t i = 0; i < entityMax; i++) {
		//	Serial.print(i + " is the index of the current sprite im drawing");

		//	(entities+1 )->DrawAtPosition(i * 10, i * 10, lcd);
		//}
	};

	//will eventually need a bounds object that stores the values of it within the world
	//these will be used for lerp only
	int maxBounds = 320;
	int minBounds = -320;

	void InitializeBounds() {

	}

	void UpdateParallax(int worldPos) {
		/*
			the system will need to have a few things kept track of
			one will be the current progess on your path
			the other needs to be the pos of the world.
			i need to be able to show that you are moving between biomes.
			maybe each area will have some bounds, and the paralax will update based on
			your position within those bounds
			basically lerp between min bound and max bound, using your current x,
			Y will determine the bounds min and max?
			these values will be calculated and stored whenever the screen turns on, which will then call update
			to draw in the correct positions.
			then this method is called whenever the system records step data.
			when you hit the end of a biome it will play a fade transition into the next biome?
		*/

		//first clamp worldpos
		if (worldPos > maxBounds) {
			worldPos = maxBounds;
		} else if (worldPos < minBounds) {
			worldPos = minBounds;
		}
		//Serial.println("///////////////////////////////////////////////");

		//normalize it
		float newPos = (float)(worldPos - minBounds) / (maxBounds - minBounds);
		//Serial.println(newPos);


		//lerp it, this is wrong. the world pos is not a 0-1 value.
		float pos = lerp(minBounds, maxBounds, newPos);
		//Serial.println(pos);


		//move layers based on this: close moves for every 2, far for every 5
		for (size_t i = 0; i < entityMax; i++) {
			//Serial.println(i + " is the index of the current sprite im drawing ");
			switch ((entities + i)->layer)
			{
			case ParallaxClose:
				(entities + i)->posX = (entities + i)->originX + pos / 2;
				break;
			case ParallaxFar:
				(entities + i)->posX = (entities + i)->originX + pos / 6;
				break;
			case Foreground:
				(entities + i)->posX = (entities + i)->originX + pos / 4;
				break;
			default:
				break;
			}
		}

	}

	float lerp(float v0, float v1, float t) {
		return (1 - t) * v0 + t * v1;
	}
};


#endif

