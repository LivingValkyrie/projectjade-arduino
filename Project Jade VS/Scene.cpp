// 
// 
// 

#include "Scene.h"

void Scene::DrawScene(Display* lcd) {
	//Serial.println("draw scene called in scene");
	for (int j = ParallaxFar; j >= GUI; j--) {
		//Serial.println("in j");
		for (size_t i = 0; i < entityMax; i++) {
			//Serial.println(i + " is the index of the current sprite im drawing ");
			if ((entities + i)->layer == j) {
				//Serial.println("layer is ");
				(entities + i)->DrawAtPosition((entities + i)->posX, (entities + i)->posY, lcd);
				//lcd->tft.displayBuffer();
				//delay(250);
			}

		}
	}

	//lcd->tft.fillScreen(ST7735_BLACK);
}

