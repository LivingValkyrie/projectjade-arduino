// GameLogic.h

#ifndef _GAMELOGIC_h
#define _GAMELOGIC_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

enum MenuState {
	Empty = 0,
	PetStats = 1,
	Train = 2,
	Connect =3,
	Map=4,
	Items=5,
	Camp=6,
	First=Empty,
	Last=Camp
};



#endif

