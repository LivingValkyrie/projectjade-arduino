#include "Input.h"

namespace Input {
	int buttonCount = 7;
	int buttonPins[6] = { INPUT_A, INPUT_B, INPUT_C, INPUT_LEFT, INPUT_UP, INPUT_DOWN };
	//int buttonPins[6] = {GPIO_2,GPIO_4,GPIO_12, GPIO_13,GPIO_14,GPIO_15 };

	char previousState = 0b00000000;
	char currentState = 0b00000000;
}

void Input::Update() {
	previousState = currentState;
	currentState = ReadPins();
}

void Input::Init() {
	currentState = 0x00000000;
	previousState = currentState;

	SetupPins();
}

int Input::GetAllPins() {
	return currentState;
}

/// <summary>
/// needs invert removed when rev 6 board is build and logic gets inverted
/// </summary>
/// <param name="id"></param>
/// <returns></returns>
bool Input::ButtonPressed(ButtonID id) {
	if (!((id & previousState) == id)) {
		//previous frame button was pressed, button is held not first press
		return false;
	}

	return !((id & currentState) == id);
}

bool Input::ButtonPressedOrHeld(ButtonID id) {
	if (Input::ButtonPressed(id) || Input::ButtonHeld(id)) {
		return true;
	} else {
		return false;
	}
}

/// <summary>
/// needs invert removed when rev 6 board is build and logic gets inverted
/// </summary>
/// <param name="id"></param>
/// <returns></returns>
bool Input::ButtonHeld(ButtonID id) {
	return !((id & currentState) == id);
}

/// <summary>
/// needs invert removed when rev 6 board is build and logic gets inverted
/// </summary>
/// <param name="id"></param>
/// <returns></returns>
bool Input::ButtonReleased(ButtonID id) {
	if (!((id & previousState) == id)) {
		//previous frame button was pressed
		// no invert means that itll return true if button is not pressed.
		return ((id & currentState) == id);
	}

	return false;
}

/// <summary>
/// returns 1 for pressed, 2 for held, 3 for released. 
/// 0 is returned on error (none of the 3 states returned true.)
/// </summary>
/// <param name="id"></param>
/// <returns></returns>
char Input::GetButtonState(ButtonID id) {
	if (ButtonPressed(id)) {
		return 1;
	} else if (ButtonHeld(id)) {
		return 2;
	} else if (ButtonReleased(id)) {
		return 3;
	}


	return 0;
}

void Input::SetupPins() {
	for (int i = 0; i < buttonCount; i++) {
		pinMode(buttonPins[i], INPUT_PULLUP);
	}
}

char Input::ReadPins() {
	char toReturn = 0;
	for (int i = 0; i < buttonCount; i++) {
		toReturn = toReturn | (digitalRead(buttonPins[i]) << i);
		//Serial.print("value at " + i);
		//Serial.println("toReturn is now " + toReturn);
		//Serial.println();
	}

	//Serial.println(toReturn, BIN);

	return toReturn;
}
