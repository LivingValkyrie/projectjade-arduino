#pragma once

#include "GPIO.h"

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

enum ButtonID {
	//for pulldown pinmode
	A		= 0b00000001,
	B		= 0b00000010,
	C		= 0b00000100,
	Left		= 0b00001000,
	Down	= 0b00010000,
	Up	= 0b00100000

	//for pullup pinmode
	//A = 0b00111110,
	//B = 0b0011110,
	//C = 0b111110,
	//Up = 0b1110,
	//Left = 0b110,
	//Down = 0b10
};

namespace Input { //convert to namespace

	void Update(), Init();
	int GetAllPins();

	bool ButtonPressed(ButtonID id), ButtonHeld(ButtonID id), ButtonReleased(ButtonID id), ButtonPressedOrHeld(ButtonID id);
	char GetButtonState(ButtonID id);
	//uint buttons[6] = {}


	extern int buttonCount;
	extern int buttonPins[6];
	extern char currentState, previousState;

	void SetupPins();
	char ReadPins();
};


/*
pinmode pullup
A:released
B:released
C:held
Up:held
down:held
Left:held
Right:held


pinmode pulldown with ==0 as pressed (means the logic is set to read as pressed when not and vice versa)
A:held
B:held
C:held
Up:held
down:held
Left:held
Right:held

pinmode pulldown with ==1 as pressed (logic corrected from previous example)
ALL 7 WORK WHEN PULLED HIGH. MEANING ALL PINS HAVE PULLDOWN RESISTORS.
THIS IS THE METHOD THAT WILL BE USED REV 6 AND BEYOND.
A:released
B:released
C:released
Up:released
down:released
Left:released
Right:released

*/