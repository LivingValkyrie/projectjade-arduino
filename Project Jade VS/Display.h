// Display.h

#ifndef _DISPLAY_h
#define _DISPLAY_h

#include "XTronical_ST7735.h"
#include "GPIO.h"
#include "GFX.h"

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

enum Alignment {
	AlignLeft, AlignRight, AlignCentered, AlignJustified // no idea how to manage justified.
	//maybe take total string width and screen size, get the difference then adjust spacing to match the character amount?
};

class Display {
public:
	Display(); //constructor

	void UpdateMenuText(char newText);
	void DrawMenuText();
	void SetupMenuText(int cursor);
	//void SetupMenuText();

	void
		Init(),
		Animate(),
		DrawMenuBar(),
		ColorTest(),
		DrawScene(),
		PrintText(String s, int x, int y, Alignment a);


	Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

	int FrogWidth = 53;
	int FrogHeight = 56;
	bool big = false;
	bool flip = false;

	int X = (tft.width() / 2) - (FrogWidth / 2); //centre of screen
	int Y = (tft.height() / 2) - (FrogHeight / 2);
	int frameRate = 30;
	int xSpeed = 2, ySpeed = 3;
	int menuBarStart = 129;
	int skyBandHeight = 36;
	int background = 88;
	int groundPlane = 16;

	char menuText = 'a';

	const int freq = 5000;
	const int ledChannel = 0;
	const int resolution = 8;
	int brightness = 255;

	void setupPWM() {
		// configure LED PWM functionalitites
		ledcSetup(ledChannel, freq, resolution);

		// attach the channel to the GPIO to be controlled
		ledcAttachPin(LCD_BRIGHTNESS, ledChannel);
	}

	void renderScreenBrightness() {
		ledcWrite(ledChannel, brightness);

	}

};

#endif

