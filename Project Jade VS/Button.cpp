// 
// 
// 

#include "Button.h"

Button::Button(int pinNumber) {
	this->pinNumber = pinNumber;
	pinMode(pinNumber, INPUT_PULLUP);
}

bool Button::Pressed() {
	if (digitalRead(pinNumber) == 0 & !pressed) {
		pressed = true;

		return true;
	} else if (digitalRead(pinNumber) == 1 & pressed) {
		pressed = false;
	}

	return false;
}

bool Button::Held() {
	if (digitalRead(pinNumber) == 0) {
		pressed = true;
		return true;
	} else if (digitalRead(pinNumber) == 1 & pressed) {
		pressed = false;
	}

	return false;
}

String Button::State() {
	String toReturn;

	if (Pressed()) {
		toReturn = "pressed";
	} else if (Held()) {
		toReturn = "held";
	} else {
		toReturn = "released";
	}


	return toReturn;
}

uint16_t Button::StateAsColor() {
	uint16_t toReturn;

	if (Pressed()) {
		toReturn = 0x001F;
	}
	else if (Held()) {
		toReturn = 0x07E0;
	}
	else {
		toReturn = 0xF800;
	}


	return toReturn;
}

bool Button::Released() {
	if (digitalRead(pinNumber) == 1 & pressed) {
		pressed = false;

		return true;
	}

	return false;
}
