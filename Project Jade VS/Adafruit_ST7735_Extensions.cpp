#include "Adafruit_ST7735.h"

#if defined (SPI_HAS_TRANSACTION)
static SPISettings mySPISettings;
#elif defined (__AVR__) || defined(CORE_TEENSY)
static uint8_t SPCRbackup;
static uint8_t mySPCR;
#endif

#ifdef SCREEN_BUFFER
void Adafruit_ST7735::displayBuffer() {
	// writes the contents of the buffer directly to the display as fast as it can!
	// At time of writing could be improved for speed by going directly to SPI interface
	// itself but for now fast enough doing it this way for most needs

	// Because we are only ever writing the screen buffer we do not need to set
	// a screen start location as by default after a reset it will be 0,0 and after
	// a full buffer write it will reset back to this. If you decide to extend this
	// code by doing partial buffer writes then you will need to set the start location
	// to 0,0 using the following line (uncomment if needed)
	  setAddrWindow(0,0,1,1);

	// we make no check for hardware spi (unlike the default code this is based on)
	// because the whole point of this is to transfer the data as fast as possible

	setAddrWindow(0, 0, ST7735_TFTWIDTH, ST7735_TFTHEIGHT);
	SPI.beginTransaction(mySPISettings);
	DC_HIGH();
	CS_LOW();

	// now quick as possible blit the data to screen
	for (int i = 0; i < ST7735_TFTWIDTH*ST7735_TFTHEIGHT; i++)
	{
		spiwrite(ScreenBuffer[i] >> 8); // send MSB first
		spiwrite(ScreenBuffer[i]);    // then LSB second
	}

	CS_HIGH();
	SPI.endTransaction();
}
#endif
//end XTronical additions

#pragma region Buffer Drawing methods
void Adafruit_ST7735::invertDisplay(boolean i) {
	writecommand(i ? ST7735_INVON : ST7735_INVOFF);
}

void Adafruit_ST7735::writecommand(uint8_t c) {
#if defined (SPI_HAS_TRANSACTION)
	if (hwSPI)    SPI.beginTransaction(mySPISettings);
#endif
	DC_LOW();
	CS_LOW();

	spiwrite(c);

	CS_HIGH();
#if defined (SPI_HAS_TRANSACTION)
	if (hwSPI)    SPI.endTransaction();
#endif
}

inline void Adafruit_ST7735::spiwrite(uint8_t c) {

	if (hwSPI) {
#if defined (SPI_HAS_TRANSACTION)
		SPI.transfer(c);
#elif defined (__AVR__) || defined(CORE_TEENSY)
		SPCRbackup = SPCR;
		SPCR = mySPCR;
		SPI.transfer(c);
		SPCR = SPCRbackup;
#elif defined (__arm__)
		SPI.setClockDivider(21); //4MHz
		SPI.setDataMode(SPI_MODE0);
		SPI.transfer(c);
#endif
	}
	else {

		// Fast SPI bitbang swiped from LPD8806 library
		for (uint8_t bit = 0x80; bit; bit >>= 1) {
#if defined(USE_FAST_IO)
			if (c & bit) *dataport |= datapinmask;
			else        *dataport &= ~datapinmask;
			*clkport |= clkpinmask;
			*clkport &= ~clkpinmask;
#else
			if (c & bit) digitalWrite(_sid, HIGH);
			else        digitalWrite(_sid, LOW);
			digitalWrite(_sclk, HIGH);
			digitalWrite(_sclk, LOW);
#endif
		}
	}
}

void Adafruit_ST7735::fillScreen(uint16_t color) {

	// XTronical additions 
#ifdef SCREEN_BUFFER
	setAddrWindow(0, 0, ST7735_TFTWIDTH, ST7735_TFTHEIGHT);
	for (int i = 0; i < ST7735_TFTWIDTH*ST7735_TFTHEIGHT; i++)
		ScreenBuffer[i] = color;
#else
	fillRect(0, 0, _width, _height, color);
#endif
	// end XTronical additions
}

void Adafruit_ST7735::setAddrWindow(uint8_t x0, uint8_t y0, uint8_t x1,	uint8_t y1) {

	writecommand(ST7735_CASET); // Column addr set
	writedata(0x00);
	writedata(x0 + xstart);     // XSTART 
	writedata(0x00);
	writedata(x1 + xstart);     // XEND

	writecommand(ST7735_RASET); // Row addr set
	writedata(0x00);
	writedata(y0 + ystart);     // YSTART
	writedata(0x00);
	writedata(y1 + ystart);     // YEND

	writecommand(ST7735_RAMWR); // write to RAM
}

void Adafruit_ST7735::writedata(uint8_t c) {
#if defined (SPI_HAS_TRANSACTION)
	if (hwSPI)    SPI.beginTransaction(mySPISettings);
#endif
	DC_HIGH();
	CS_LOW();

	spiwrite(c);

	CS_HIGH();
#if defined (SPI_HAS_TRANSACTION)
	if (hwSPI)    SPI.endTransaction();
#endif
}
#pragma endregion

#pragma region LowLevel Bit working


inline void Adafruit_ST7735::CS_HIGH(void) {
#if defined(USE_FAST_IO)
	*csport |= cspinmask;
#else
	digitalWrite(_cs, HIGH);
#endif
}

inline void Adafruit_ST7735::CS_LOW(void) {
#if defined(USE_FAST_IO)
	*csport &= ~cspinmask;
#else
	digitalWrite(_cs, LOW);
#endif
}

inline void Adafruit_ST7735::DC_HIGH(void) {
#if defined(USE_FAST_IO)
	*dcport |= dcpinmask;
#else
	digitalWrite(_dc, HIGH);
#endif
}

inline void Adafruit_ST7735::DC_LOW(void) {
#if defined(USE_FAST_IO)
	*dcport &= ~dcpinmask;
#else
	digitalWrite(_dc, LOW);
#endif
}
#pragma endregion

void Adafruit_ST7735::drawPixel(int16_t x, int16_t y, uint16_t color) {

	if ((x < 0) || (x >= _width) || (y < 0) || (y >= _height)) return;


#ifdef SCREEN_BUFFER      //XTronical additions
	// write to screen buffer memory instead, this is quick and dirty, presumes always using
	// RGB565 (16bit per pixel colour)
	// Calculate memory location based on screen width and height
	ScreenBuffer[y * ST7735_TFTWIDTH + x] = color;
#else
	setAddrWindow(x, y, x + 1, y + 1);

	#if	defined (SPI_HAS_TRANSACTION)
		if (hwSPI)     SPI.beginTransaction(mySPISettings);
	#endif

	DC_HIGH();
	CS_LOW();
	spiwrite(color >> 8);
	spiwrite(color);
	CS_HIGH();

	#if defined (SPI_HAS_TRANSACTION)
		if (hwSPI)     SPI.endTransaction();
	#endif
#endif     //end XTronical additions

}