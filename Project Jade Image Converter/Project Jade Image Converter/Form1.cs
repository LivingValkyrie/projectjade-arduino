﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Project_Jade_Image_Converter {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
            InitDefaults();
        }

        private void InitDefaults() {
            petDefault = new Bitmap(Application.StartupPath + "\\Resources\\Pet.png");
            sceneryDefault = new Bitmap(Application.StartupPath + "\\Resources\\Scenery.png");
            //pictureBox1.Image = petDefault;
        }

        string[] hexCodes;
        Bitmap petDefault, sceneryDefault;
        private void LoadFromFile() {
            if(openFileDialog1.ShowDialog() == DialogResult.OK) {
                Bitmap img = new Bitmap(openFileDialog1.FileName);
                hexCodes = new string[img.Width * img.Height];
                for(int i = 0; i < img.Width; i++) {
                    for(int j = 0; j < img.Height; j++) {
                        Color pixel = img.GetPixel(i, j);
                        int coord = j * img.Width + i;
                        hexCodes[coord] = RGBTo565(pixel);
                    }
                }

                pictureBox1.Image = img;
                //pictureBox1.Width = img.Width;
                //pictureBox1.Height = img.Height;

                //SaveAsHexcode(hexCodes);
            }
        }

        private void SaveAsHexcode(string[] hexCodes, string fileName) {
            using(StreamWriter s = new StreamWriter(fileName + ".hexCode")) {
                s.Write("const unsigned short Graphic[" + hexCodes.Length + "] PROGMEM = {\n");
                for(int i = 0; i < hexCodes.Length; i++) {
                    string item = hexCodes[i];
                    if(i % 15 == 0 && i > 0) { //for some reason off by 1 on first line
                        s.Write(item + ",\n");
                    }
                    else if(i == hexCodes.Length - 1) {
                        s.Write(item + "\n};");
                    }
                    else {
                        s.Write(item + ",");
                    }
                }
            }
        }

        SpriteSheetType currType = SpriteSheetType.Pet;
        void LoadDefaults(SpriteSheetType sheetType) {
            switch(sheetType) {
                case SpriteSheetType.Pet:
                    pictureBox1.Image = petDefault;
                    break;
                case SpriteSheetType.Scenery:
                    pictureBox1.Image = sceneryDefault;

                    break;
                default:
                    break;
            }   
        }

        private void RGBTo565(int r, int g, int b, out int x1, out int x2) {
            x1 = (r & 0xF8) | (g >> 5); // Take 5 bits of Red component and 3 bits of G component       
            x2 = ((g & 0x1C) << 3) | (b >> 3);
        }
        private string RGBTo565(Color color) {
            int x1, x2, r, g, b;

            r = color.R;
            g = color.G;
            b = color.B;

            x1 = (r & 0xF8) | (g >> 5); // Take 5 bits of Red component and 3 bits of G component

            x2 = ((g & 0x1C) << 3) | (b >> 3);

            return "0x" + x1.ToString("X2") + x2.ToString("X2");
        }

        private void SheetSelectionBox_SelectedIndexChanged(object sender, EventArgs e) {
            switch(sheetSelectionBox.SelectedIndex) {
                case 0: //pet
                    animationContainer.Text = "Pet Animations";
                    currType = SpriteSheetType.Pet;
                    break;
                case 1: //scenery
                    animationContainer.Text = "Scenery Paralax";
                    currType = SpriteSheetType.Scenery;
                    break;
                default:
                    break;
            }
        }

        private void ButtonOpen_Click(object sender, EventArgs e) {
            LoadFromFile();
        }

        private void ButtonNew_Click(object sender, EventArgs e) {
            LoadDefaults(currType);
        }

        private void ButtonExport_Click(object sender, EventArgs e) {
            SaveAsHexcode(hexCodes, fileNameTextbox.Text);
        }
    }

    public enum SpriteSheetType {
        Pet,
        Scenery
    }
}
