﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpriteGenerator {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            ImageConverter.imgPath = "Tester.png";
            ImageConverter.LoadFromFile();

            Console.ReadLine();

            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
        }
    }


    static class ImageConverter {
        public static string imgPath = "";
        static string[] hexCodes;
        static int[] pixelIndexes;
        static List<String> pallette = new List<string>();
        static int width;

        public static void LoadFromFile() {

            Bitmap img = new Bitmap(imgPath);
            hexCodes = new string[img.Width * img.Height];
            width = img.Width;
            for (int i = 0; i < img.Width; i++) {
                for (int j = 0; j < img.Height; j++) {
                    Color pixel = img.GetPixel(i, j);
                    int coord = j * img.Width + i;
                    hexCodes[coord] = RGBTo565(pixel);
                }
            }

            PrintHexcodes();
            GetPallette();
            GetPixelIndexes();
        }

        static void GetPallette() {
            foreach (var hex in hexCodes) {
                if (!pallette.Contains<string>(hex)) {
                    pallette.Add(hex);
                }
            }

            Console.WriteLine("Pallette contains the following: ");
            foreach (var s in pallette) {
                Console.Write($"{s} ");

            }

            Console.WriteLine();
        }

        static void GetPixelIndexes() {
            pixelIndexes = new int[hexCodes.Length];

            for (int i = 0; i < hexCodes.Length; i++) {
                pixelIndexes[i] = GetIndexViaColor(hexCodes[i]);
            }

            PrintPixelIndexes();

        }

        static int GetIndexViaColor(string color) {
            for (int i = 0; i < pallette.Count; i++) {
                if (color == pallette[i]) {
                    return i;
                }
            }

            return -1;
        }

        static void PrintPixelIndexes() {
            for (int i = 0; i < pixelIndexes.Length; i++) {
                string s = pixelIndexes[i].ToString();

                if (i == 0) {
                    Console.Write($"{s} ");

                } else {

                    if (i % width == 0) {
                        Console.Write($"\n{s} ");
                    } else {
                        Console.Write($"{s} ");
                    }
                }
            }

            Console.WriteLine();
        }

        static void PrintHexcodes() {
            for (int i = 0; i < hexCodes.Length; i++) {
                string s = hexCodes[i];

                if (i == 0) {
                    Console.Write($"{s} ");

                } else {

                    if (i % 10 == 0) {
                        Console.Write($"\n{s} ");

                    } else {
                        Console.Write($"{s} ");
                    }
                }
            }

            Console.WriteLine();
        }

        private static string RGBTo565(Color color) {
            int x1, x2, r, g, b;

            r = color.R;
            g = color.G;
            b = color.B;

            x1 = (r & 0xF8) | (g >> 5); // Take 5 bits of Red component and 3 bits of G component

            x2 = ((g & 0x1C) << 3) | (b >> 3);

            return "0x" + x1.ToString("X2") + x2.ToString("X2");
        }


    }
}
